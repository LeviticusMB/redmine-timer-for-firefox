var timingproject = "";

function scopepreserver(mypath) {
	return function () {
		console.log("ID: " + mypath);
		
		if (timingproject != "") {
			// Something is timing already. Make it stop.
			self.port.emit("startstop", "", false);
			
			// Turn the button off
			document.getElementById(timingproject).className = "bigdiv";
			
			// Remove the timer div's text and itself
			document.getElementById("timerdiv").removeChild(document.getElementById("timerdiv").firstChild);
			document.getElementById(timingproject).removeChild(document.getElementById("timerdiv"));
		}
		if (timingproject != mypath) {
			// They started some new project (whether another was already recording or not). Start recording.
			self.port.emit("startstop", mypath, true);
			timingproject = mypath;
			document.getElementById(timingproject).className += " recording";
			
			var timediv = document.createElement("div");
			timediv.setAttribute("id","timerdiv");
			var d = new Date();
			var t = d.toLocaleTimeString();
			var timeText = document.createTextNode("Started at " + t);
			timediv.appendChild(timeText);
			
			document.getElementById(timingproject).appendChild(timediv);
			
		} else {
			// They just turned off the same project that was recording. Reset variables.
			timingproject = "";
		}
	};
}

self.port.on("sendprojects", function(projects) {
	// Clean out all the existing projects
	var projectlist = document.getElementById("projectlist");
	while (projectlist.firstChild) {
		// Remove the listener 
		projectlist.firstChild.removeEventListener("click",scopepreserver());
		
		// Remove the text inside the div
		projectlist.firstChild.removeChild(projectlist.firstChild.firstChild);
		
		// Remove the div
		projectlist.removeChild(projectlist.firstChild);
	}
	
	// Put things in alphabetical order
	projects.sort(function(first, second){
		var project1 = first.projectname.toLowerCase();
		var project2 = second.projectname.toLowerCase();
		
		if (project1 < project2) 
			{ return -1; }
		if (project1 > project2)
			{ return 1; }
		return 0; 
	});

	// list all the projects
	for (var i = 0; i < projects.length; i++) {
		var newdiv = document.createElement("div");
		var linkText = document.createTextNode(projects[i].projectname);
		newdiv.appendChild(linkText);
		newdiv.setAttribute("id",projects[i].projectid);
		newdiv.setAttribute("class","bigdiv");
		newdiv.addEventListener("click", scopepreserver(projects[i].projectid));
			
		// Add the link to the parent div
		document.getElementById("projectlist").appendChild(newdiv);
	}
});

self.port.on("divstyle", function(buttonsize) {
	// See how big they want their buttons to be (from preferences)
	var divstyle =  "width: 275px; height: 110px; font-size: 1.2em; padding: 17px;";
	if (buttonsize==1) { divstyle="width: 200px; height: 80px; font-size: 1em; padding: 8px;"; }
	if (buttonsize==2) { divstyle="width: 140px; height: 50px; font-size: 0.7em; padding: 3px;"; }
	
	var css = document.createElement("style");
	css.type = "text/css";
	css.innerHTML = ".bigdiv { " + divstyle + " }";
	document.head.appendChild(css);
});