var projects = document.getElementsByClassName("project");
var i;

// Get the list of projects already added
self.port.on("sendprojects2", function(projects) {
	// Relabel the ones they've already added
	for (var i = 0; i < projects.length; i++) {
		document.getElementById(projects[i].projectid).innerHTML = "Remove";
	}
});

// Add a link to show the time page
function openthelist() {
	return function () {
		//console.log("Sending signal to open the list");
		self.port.emit("openlist", "");
	};
}

var contextmenu = document.getElementsByClassName("contextual")[0];
var spacerline = document.createTextNode(" | ");
var opentab = document.createElement("a");
var opentablinkText = document.createTextNode("Open my timing buttons");
opentab.appendChild(opentablinkText);
//opentab.setAttribute("style", "margin-left: 10px;");
opentab.addEventListener("click", openthelist());
contextmenu.appendChild(spacerline);
contextmenu.appendChild(opentab);

// The scope preservation thing melted my brain for hours... it's a "closure". Read this: http://www.howtocreate.co.uk/referencedvariables.html
function scopepreserver(mypath,myproject) {
	return function () {
		//console.log("ID and name: " + mypath + " --- " + myproject);
		self.port.emit("addproject", mypath, myproject);
		
		// Change it to "remove"
		var sendinglink = document.getElementById(mypath);
		if (sendinglink.innerHTML=="Remove") {
			// Change it back to "Add to Timer Buttons"
			sendinglink.innerHTML = "Add to Timer Buttons";
		} else {
			sendinglink.innerHTML = "Remove";
		}
	};
}

for (i = 0; i < projects.length; i++) {
	
	var newlink = document.createElement("a");
	var linkText = document.createTextNode("Add to Timer Buttons");
	newlink.appendChild(linkText);
	newlink.setAttribute("class", "rtff-addproject");
	
	var pathsplit = projects[i].href.split("/");
	
	newlink.setAttribute("id", pathsplit[4]);
	
	// This doesn't work because it's evaluated in the page context. See: https://developer.mozilla.org/en-US/Add-ons/SDK/Guides/Content_Scripts
	//newlink.setAttribute("onclick", "self.port.emit(\"addproject\"," + i + ",abc)");
	newlink.addEventListener("click", scopepreserver(pathsplit[4],projects[i].innerHTML));
		
	// Add the link to the parent div
	projects[i].parentNode.appendChild(newlink);
}
