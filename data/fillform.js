self.port.on("fillhours", function(hours, activity, usercomments) {
	
  var hourfield = document.getElementById("time_entry_hours"); 
  if (hourfield) {
	hourfield.value = hours;
  }
   	
  var activityfield = document.getElementById("time_entry_activity_id"); 
  if (activityfield) {
	
	for(var i = 0;i < activityfield.options.length;i++){
		if(activityfield.options[i].value == activity ){
			activityfield.options[i].selected = true;
		}
	}
  }
  
  var commentfield = document.getElementById("time_entry_comments"); 
  if (commentfield) {
	commentfield.value = usercomments;
  }
});
