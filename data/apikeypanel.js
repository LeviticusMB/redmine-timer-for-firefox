addon.port.on("show", function onShow(havekey) {
	if (havekey) {
		document.getElementById("replace").style.display = "block";
	}
});

function save(saveit) {
	//alert("API: " + document.getElementById("theapikey").value);
	if (document.getElementById("theapikey").value=="" && saveit==true) {
		alert("No API key entered. We will NOT replace the existing key, if any.");
	}
	
	if (saveit) {
		addon.port.emit("savekey", document.getElementById("theapikey").value);
	} else {
		addon.port.emit("savekey", "");
	}
}
