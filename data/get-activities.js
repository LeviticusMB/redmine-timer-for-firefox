// When the user hits a button, send message to main.js.
// The message payload is the selected value.
var activitylist = document.getElementById("activitylist");

function save(selectedid) {
	addon.port.emit("selectedid", selectedid);
}

// Listen for the "show" event being sent from the
// main add-on code. It means that the panel's about
// to be shown.
addon.port.on("show", function onShow(json) {
	//var content = "";
	
	// Clean out whatever is in there (so we don't add the list over and over each time they open the panel)
	activitylist.innerHTML = "";
	
	for(var i=0;i<json.time_entry_activities.length;i++){
        
		// Firefox review process does not like this code... putting HTML in strings is bad
		//content = content + "<p><a href='#' onclick='save(" + json.time_entry_activities[i].id + ")'>" + json.time_entry_activities[i].name + "</a></p>";
		
		
		// So instead, make a paragraph with a button in it
		var actp = document.createElement("P");
		var acta = document.createElement("A");
		acta.href = "#";
		acta.id = json.time_entry_activities[i].id;
		acta.onclick = function() { save(this.id); };
		var acttext = document.createTextNode(json.time_entry_activities[i].name);
		
		// Put text in the link, and the link in the P (button), and the button in the list
		acta.appendChild(acttext);
		actp.appendChild(acta);
		activitylist.appendChild(actp);
    }
	
	//activitylist.innerHTML = content;
	
});