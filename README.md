# README #

This is a time tracking plugin for Firefox.  It reads the Redmine page you're on when you start timing, and it logs time accordingly.  As of version 0.3, it gives you a page full of buttons you can use to track time.

To build the code, install JPM and the Mozilla SDK. Instructions here: https://developer.mozilla.org/en-US/Add-ons/SDK/Tutorials/Installation

The Redmine API documentation is here: http://www.redmine.org/projects/redmine/wiki/Rest_TimeEntries

Icons made by Freepik from www.flaticon.com and are licensed by CC BY 3.0
http://www.flaticon.com/authors/freepik
http://www.flaticon.com