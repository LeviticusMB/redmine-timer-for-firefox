

// TODOs: Attach some kind of minute converter to both log entry & edit pages. 

// ********** Variables *********** //

var buttons = require("sdk/ui/button/action");
var tabs = require("sdk/tabs");
var preferences = require("sdk/simple-prefs").prefs;
var notifications = require("sdk/notifications");
var self = require("sdk/self");
var data = require("sdk/self").data;
var { Hotkey } = require("sdk/hotkeys");
var { setTimeout, clearTimeout } = require("sdk/timers");
var timeoutid = 0;
var reminderinterval = 1800000; // 30 minutes
var recording = false;
var starttime;
var issue = "";
var project = "";
var redmineprojectID = 0;
var comments = "";
var gotcomments = false;
var myIconURL = "./timer64.png";
var apikey = getapikey();

// ********** Buttons *********** //

var startorstop = Hotkey({
  combo: "alt-r",
  onPress: function() {
    handleClick(false);
  }
});

var button = buttons.ActionButton({
  id: "redmine-timer",
  label: "Start recording",
  icon: {
    "16": "./timer16.png",
	"20": "./timer20.png",
	"24": "./timer24.png",
    "32": "./timer32.png",
    "64": "./timer64.png"
  },
  onClick: handleClick
});

// ********** Click Handler *********** //

function handleClick(state, projectbutton) {
	// Are we already recording time? 
	if (recording) {
		//console.log("Stopping the timer. Logging to Redmine.");
		
		// Deal with comments
		if (!preferences.getcomments) {
			// This would only apply if they had comments on and entered one, then later turned it off
			// This keeps it from using their last comment over and over
			comments = "";
		}
		
		if (preferences.getcomments && gotcomments==false) {
			text_entry.show();
			gotcomments=true;
			return;
		} else {
			gotcomments = false;
		}
		
		// Get the time difference
		var stoptime = new Date(); 
		var totaltime = stoptime - starttime;
		//console.log("Total time in milliseconds: " + totaltime)
		totaltime = Math.round(((totaltime / 1000 / 60 / 60) + 0.00001) * 100) / 100;
		//console.log("Total time in hours: " + totaltime)
		
		// Issue or project?
		var issueorprojectid = "";
		var notificationnote = "";
		
		if (issue!="") {
			issueorprojectid = "\"issue_id\":"+issue+",";
			notificationnote = "Issue #" + issue;
		} else if (project!="") {
			issueorprojectid = "\"project_id\":\""+redmineprojectID+"\",";
			notificationnote = "Project identifier = " + project + " " + redmineprojectID;
		} else {
			// open new tab with form
			//console.log("Activity is: " + preferences.activity);
			
			// TODO: Attach minute converter to this page
			tabs.open({
			  url: getredmineurl()+"/time_entries/new",
			  onReady: function() {
				var worker = tabs.activeTab.attach({
					contentScriptFile: self.data.url("fillform.js")
					});
				worker.port.emit("fillhours", totaltime, preferences.activity, comments);
				
				}
			});
						
			stopandcleanup();
			return;
		}
		
		// Create submission for Redmine
		var toredmine = "{\"time_entry\": {"+issueorprojectid+"\"hours\":"+totaltime+",\"activity_id\":"+preferences.activity+",\"comments\":\""+comments+"\"}}";
		//console.log("For Redmine: " + toredmine);
		
		// Submit to Redmine
		var Request = require("sdk/request").Request;
		var sendtoredmine = Request({
			url: getredmineurl()+"/time_entries.json",
			contentType: "application/json",
			headers: {
				"X-Redmine-API-Key": currentapikey(),
			},
			content: toredmine,
			onComplete: function (response) {
				//console.log("Header status code: " + response.status);
				//console.log("Response: " + response.text);
				
				if (response.status!=201) {
					//console.log("Error logging time for some reason.");
					notifications.notify({
						title: "Uh oh",
						iconURL: myIconURL,
						text: "Something went wrong sending this to Redmine. Make sure you're online, your API key is valid, and this project has the Time Tracking module enabled in Redmine."
					});
				} else {
					//console.log("Submitted ok. Switch the icon and notify user.");
					
					var timeentryid = 0;
					var jsonresponse = response.json;
					if (jsonresponse) {
						timeentryid = jsonresponse.time_entry.id;
					}
					//console.log("Time entry ID: " + timeentryid);
					
					notifications.notify({
						title: "Time recorded",
						iconURL: myIconURL,
						text: "Recorded "+Math.round(totaltime*60)+" minutes for "+notificationnote+". Click to edit.",
						data: timeentryid.toString(),
						  onClick: function (data) {
							//console.log("They clicked the notification. Send to edit time log " + data);
							// TODO: Attach minute converter to this page
							tabs.open({
							  url: getredmineurl()+"/time_entries/" + data + "/edit"
							});
						  }
					});
				}
				
			}
		}).post();
		
		stopandcleanup();
			
		
	} else { // We are not recording, so start
		
		// Make sure they have an API key
		if (currentapikey()=="") {
			//console.log("They didn't put in an API key.");
			notifications.notify({
				title: "No API Key",
				iconURL: myIconURL,
				text: "Please put in your API key. You can get this from your User page in Redmine."
			});	
			
			// exit function
			return;
		}
		
		// record the start time
		starttime = new Date(); 
		
		// figure out what they want to record
		if (projectbutton) {
			// They tapped a button
			project = projectbutton;
			
			//console.log("Got project from button: " + project);
			
			// Get the Redmine ID to use later
			getProjectID(project);
		} else {
			// Figure it out from the URL, if possible
			//console.log('active: ' + tabs.activeTab.url);
		  
			var url = require("sdk/url").URL(tabs.activeTab.url);
			//console.log(url.path);  
			
			var path = url.path;
			if (path.indexOf("/issues/")>-1) {
				//console.log("Start timing on an issue");
				var pathsplit = path.split("/");
				issue = pathsplit[2];
				//console.log("Issue ID: " + issue);
					
				
			} else if (path.indexOf("/projects/")>-1) {
				//console.log("Start timing on a project");
				var pathsplit = path.split("/");
				project = pathsplit[2];
				
				// Check for querystrings and hashes, since that breaks things
				if (project.indexOf("?")>-1) {
					project = project.substr(0,project.indexOf("?"));
				}
				if (project.indexOf("#")>-1) {
					project = project.substr(0,project.indexOf("#"));
				}
				
				//console.log("Project ID: " + project);
				
				// Get the Redmine ID to use later
				getProjectID(project);
			} else {
				//console.log("Don't know what they want to record.  Will show form later.");
						
			}
		}
		
		//console.log("Started timer. Switch the icon now.");
		button.badge = "rec";
		button.label = "Started " + starttime.getHours() + ":" + starttime.getMinutes();
		recording = true;
		// Stop old timer and create a new one
		killreminder(timeoutid);
		//console.log("Starting a new reminder (to stop recording) for " + reminderinterval + " milliseconds");
		timeoutid = setTimeout(function() { makereminder(false); }, reminderinterval);
		
	} // if not recording
}

// ****** Helper functions ******
function stopandcleanup() {
	// Clean up
	//console.log("Cleaning up");
	button.badge = null;
	button.label = "Log time";
	recording = false;
	issue = "";
	project = "";
	
	// Stop old timer and create a new one
	killreminder(timeoutid);
	//console.log("Creating a new reminder (start recording) in " + reminderinterval + " milliseconds");
	timeoutid = setTimeout(function() { makereminder(true); }, reminderinterval);
}

function getredmineurl() {
	var str = preferences.redmineurl;
	if(str.substr(-1) == '/') {
        return str.substr(0, str.length - 1);
    }
    return str;
}

function getapikey() {
	
	require("sdk/passwords").search({
		url: require("sdk/self").uri,
		realm: "Redmine API Key",
		onComplete: function onComplete(credentials) {
		  credentials.forEach(function(credential) {
			//console.log("Found API key: " + credential.password);
			apikey = credential.password;
			
			});
		  },
		onError: function onError() { 
			//console.log("Could not find API key");
			apikey = ""; 
		}
    });
	
}

function currentapikey() {
	// Jumping through hoops here to store this API key as a password
	// Have to be able to return the variable anew each time
	//console.log("Returning API key: " + apikey);
	return apikey;
}

// ****** Timers ******
function makereminder(torecord) {
	if (torecord) {
		notifications.notify({
			title: "Record time",
			iconURL: myIconURL,
			text: "Don't forget to record your time!"
		});
	} else {
		notifications.notify({
			title: "Still working?",
			iconURL: myIconURL,
			text: "You have been recording for a long time. Don't forget to stop recording when finished."
		});
	}
}

function killreminder(id) {
	//console.log("Action happened -- killing reminder");
	clearTimeout(id);
}

// Set initial reminder to log time
// TODO: This should probably be a setInterval rather than a setTimeout, but we'd need some way to stop it from bugging them after hours
//console.log("Setting the initial reminder");
timeoutid = setTimeout(function() { makereminder(true); }, reminderinterval);

// ****** Activity Type picker ******
var activitypanel = require("sdk/panel").Panel({
	height: 500,
	contentURL: data.url("activitypanel.html")
});

var sp = require("sdk/simple-prefs");
sp.on("activitylist", function() {
	//console.log("Showing the activity list panel");
	activitypanel.show();
});

activitypanel.on("show", function() {
	var Request = require("sdk/request").Request;
	var redminerequest = Request({
		url: getredmineurl()+"/enumerations/time_entry_activities.json",
		contentType: "application/json",
		headers: {
			"X-Redmine-API-Key": currentapikey(),
		},
		
		onComplete: function (response) {
			//console.log("Header status code: " + response.status);
			//console.log("Response: " + response.text);
			
			if (response.status!=200) {
				//console.log("Error getting activities list from Redmine.");
				notifications.notify({
					title: "Uh oh",
					iconURL: myIconURL,
					text: "Something went wrong checking the activity list with Redmine. Make sure you're online and that your API key is valid."
				});
			} else {
				//console.log("Got the list from Redmine.");
				activitypanel.port.emit("show",response.json);
			}
			
		}
	}).get();
});

activitypanel.port.on("selectedid", function (activityid) {
	if (activityid>0) {
		//console.log("Activity ID: " + activityid);
		preferences.activity = activityid.toString();
	} else {
		//console.log("Nothing chosen");
	}
	
	activitypanel.hide();
});

// ****** API key entry ******
var apikeypanel = require("sdk/panel").Panel({
	height: 300,
	contentURL: data.url("apikeypanel.html")
});

sp.on("apikey", function() {
	//console.log("Showing the API key panel");
	apikeypanel.show();
});

apikeypanel.on("show", function() {
	//console.log("Telling the API key panel whether we already have a key");
	var havekey = true;
	if (currentapikey()=="" || currentapikey()==undefined) { havekey = false; }
	apikeypanel.port.emit("show",havekey);
});

apikeypanel.port.on("savekey", function (apikeyentry) {
	if (apikeyentry!="") {
		//console.log("Got an API key -- storing it as: " + apikeyentry);
		// Remove old passwords first
		require("sdk/passwords").search({
		  realm: "Redmine API Key",
		  onComplete: function onComplete(credentials) {
			credentials.forEach(require("sdk/passwords").remove);
			require("sdk/passwords").store({
			realm: "Redmine API Key",
			username: "apikey",
			password: apikeyentry,
			});
			getapikey();
		  }
		});
		
		
	} else {
		//console.log("Nothing entered");
	}
	
	apikeypanel.hide();
});

// ****** Comments panel ******
var text_entry = require("sdk/panel").Panel({
  contentURL: data.url("text-entry.html"),
  contentScriptFile: data.url("get-text.js")
});

// When the panel is displayed it generated an event called
// "show": we will listen for that event and when it happens,
// send our own "show" event to the panel's script, so the
// script can prepare the panel for display.
text_entry.on("show", function() {
  text_entry.port.emit("show");
});

// Listen for messages called "text-entered" coming from
// the content script. The message payload is the text the user
// entered.
text_entry.port.on("text-entered", function (text) {
  //console.log("Comments: " + text);
  comments = text;
  text_entry.hide();
  handleClick(false);
});


// ********** Project List System *********** //
// Everything to allow users to choose the current project from big buttons rather than having to go to the page
var ss = require("sdk/simple-storage");
var pageMod = require("sdk/page-mod");
// Create simple storage if it doesn't exist
if (!ss.storage.projects) { ss.storage.projects = []; }

function showthelist() {
	return function () {
		//console.log("Open project list with " + ss.storage.projects.length + " projects");
		if (ss.storage.projects.length == 0) {
			alert("You need to add some projects first.");
		}
		var tabs = require("sdk/tabs");
		var projmod = require("sdk/page-mod");
		var projself = require("sdk/self");
		
		var pageUrl = self.data.url("projectab.html")
		
		var pageModProj = projmod.PageMod({
			include: pageUrl,
			contentScriptFile: "./projectab.js",
			onAttach: function(worker) {
				worker.port.emit("sendprojects", ss.storage.projects);
				worker.port.emit("divstyle", preferences.buttonsize);
				worker.port.on("startstop", function(projectid, start) {
					//console.log("Received start/stop command for: " + projectid + " Start?: " + start);
					if (start) {
						// pass the project ID so it can start
						handleClick(false,projectid);
					} else {
						// They're stopping, so we don't have to pass anything
						handleClick();
					}
				});
				worker.on('detach', function () {
					worker.destroy();
				});
			}
		})
		
		tabs.open(pageUrl);
		
	};
}

function getProjectID(projectName) {
	// Gets the project ID from the project URL
	// Not really sure why Redmine makes us do this... but in some versions sending the project code from the URL doesn't seem to work
	//console.log("Requesting project ID from " + getredmineurl()+"/projects/" + projectName + ".json");
	
	var Request2 = require("sdk/request").Request;
	var sendtoredmine2 = Request2({
		url: getredmineurl()+"/projects/" + projectName + ".json",
		contentType: "application/json",
		headers: {
			"X-Redmine-API-Key": currentapikey(),
		},
		content: "",
		onComplete: function (response) {
			//console.log("Header status code: " + response.status);
			//console.log("Response: " + response.text.substring(0,600));
			
			if (response.status!=200) {
				//console.log("Error getting the project ID.");
				notifications.notify({
					title: "Uh oh",
					iconURL: myIconURL,
					text: "Something went wrong getting the project ID. Make sure you're online and your API key is valid."
				});
			} else {
				redmineprojectID = response.json.project.id;
				//console.log("Got project ID = " + redmineprojectID);
				
			}
			
		}
	}).get();
	
}

var projectlistpagemod;
makeProjectPageMod();

function makeProjectPageMod() {
	projectlistpagemod = pageMod.PageMod({
	  include: getredmineurl()+"/projects",
	  contentScriptFile: "./projectlist.js",
	  attachTo: ["existing", "top"],
	  contentStyle: ".rtff-addproject { margin: 10px; font-size: 0.8em; }",
	  onAttach: function(worker) {
			worker.port.emit("sendprojects2", ss.storage.projects);
			worker.port.on("addproject", function(projectid, projectname) {
			
				//console.log("Adding: " + projectid + " Name: " + projectname);
				
				// See if the item is already there... remove if so 
				var createnew = true;
				for (var i = 0; i < ss.storage.projects.length; i++) {
					if (ss.storage.projects[i].projectid==projectid) {
						createnew = false;
						ss.storage.projects.splice(i, 1);
						//console.log("Found project and removed");
						break;
					} else {
						//console.log("Does this project exist? It's not: " + ss.storage.projects[i].projectid);
					}
				}
				
				if (createnew) {
					// Create the project object to push in
					var valueToPush = { };
					valueToPush.projectid = projectid;
					valueToPush.projectname = projectname;
					ss.storage.projects.push(valueToPush);
				}
				
			});
			worker.port.on("openlist", showthelist());
			worker.on('detach', function () {
						worker.destroy();
					});
		}
	});
}

function onPrefChange(prefName) {
  //console.log("The Redmine URL has changed to " + getredmineurl());
  
  // Must recreate the pagemod so it attaches to the right URL
  projectlistpagemod.destroy();
  makeProjectPageMod();
}
require("sdk/simple-prefs").on("redmineurl", onPrefChange);